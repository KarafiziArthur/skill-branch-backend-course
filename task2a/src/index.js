import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: "hello js"
  });
});

app.get('/task2a', (req, res) => {
  const sum = (parseInt(req.query.a) || 0) + (parseInt(req.query.b) || 0);
  res.send(sum.toString());
});

app.get('/task2b', (req, res) => {
  let fullname = req.query.fullname.replace(/\s+/g, ' ');
  fullname = fullname.replace(/^\s+|\s+$/g,'');
  fullname = fullname.split(' ');
  let firstName = fullname[0] ? fullname[0].toLowerCase() : null;
  let patronymic = fullname[1] ? fullname[1].toLowerCase() : null;
  let lastName = fullname[2] ? fullname[2].toLowerCase() : null;
  let result, invalid = false;
  let numInStr = /[0-9]/g.test(req.query.fullname);
  let symInStr = /(_|\+|\/)/g.test(req.query.fullname);

  if (firstName) firstName = firstName[0].toUpperCase() + firstName.slice(1);
  if (lastName) lastName = lastName[0].toUpperCase() + lastName.slice(1);
  if (patronymic) patronymic = patronymic[0].toUpperCase() + patronymic.slice(1);

  if (fullname.length > 3 || !req.query.fullname || numInStr || symInStr) {
    invalid = true;
  }

  if (invalid) {
    result = 'Invalid fullname';
  } else if (fullname.length === 3) {
     result = `${lastName} ${firstName.slice(0, 1)}. ${patronymic.slice(0, 1)}.`;
  } else if (fullname.length === 2) {
    result = `${patronymic} ${firstName.slice(0, 1)}.`;
  } else if (fullname.length === 1) {
    result = `${firstName}`;
  }

  res.send(result);
});

app.listen(3000, () => {
  console.log('Listening at http://localhost:3000/')
});